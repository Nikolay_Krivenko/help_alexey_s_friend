/********************************************************************************
** Form generated from reading UI file 'frmmain.ui'
**
** Created by: Qt User Interface Compiler version 5.1.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FRMMAIN_H
#define UI_FRMMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_frmMain
{
public:
    QWidget *centralWidget;
    QPushButton *cmdEdit;
    QWidget *widget;
    QVBoxLayout *verticalLayout;
    QLabel *lblFIO;
    QLabel *lblRANK;
    QLabel *lblREASON;
    QWidget *widget1;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *txtFIO;
    QComboBox *cboRANK;
    QComboBox *cboREASON;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *frmMain)
    {
        if (frmMain->objectName().isEmpty())
            frmMain->setObjectName(QStringLiteral("frmMain"));
        frmMain->resize(352, 168);
        centralWidget = new QWidget(frmMain);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        cmdEdit = new QPushButton(centralWidget);
        cmdEdit->setObjectName(QStringLiteral("cmdEdit"));
        cmdEdit->setGeometry(QRect(130, 90, 75, 23));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        widget->setGeometry(QRect(10, 10, 109, 71));
        verticalLayout = new QVBoxLayout(widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        lblFIO = new QLabel(widget);
        lblFIO->setObjectName(QStringLiteral("lblFIO"));
        QFont font;
        font.setFamily(QStringLiteral("Times New Roman"));
        font.setPointSize(11);
        lblFIO->setFont(font);

        verticalLayout->addWidget(lblFIO);

        lblRANK = new QLabel(widget);
        lblRANK->setObjectName(QStringLiteral("lblRANK"));
        lblRANK->setFont(font);

        verticalLayout->addWidget(lblRANK);

        lblREASON = new QLabel(widget);
        lblREASON->setObjectName(QStringLiteral("lblREASON"));
        QFont font1;
        font1.setFamily(QStringLiteral("Times New Roman"));
        font1.setPointSize(11);
        font1.setBold(false);
        font1.setWeight(50);
        lblREASON->setFont(font1);

        verticalLayout->addWidget(lblREASON);

        widget1 = new QWidget(centralWidget);
        widget1->setObjectName(QStringLiteral("widget1"));
        widget1->setGeometry(QRect(120, 10, 216, 74));
        verticalLayout_2 = new QVBoxLayout(widget1);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        txtFIO = new QLineEdit(widget1);
        txtFIO->setObjectName(QStringLiteral("txtFIO"));

        verticalLayout_2->addWidget(txtFIO);

        cboRANK = new QComboBox(widget1);
        cboRANK->setObjectName(QStringLiteral("cboRANK"));

        verticalLayout_2->addWidget(cboRANK);

        cboREASON = new QComboBox(widget1);
        cboREASON->setObjectName(QStringLiteral("cboREASON"));

        verticalLayout_2->addWidget(cboREASON);

        frmMain->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(frmMain);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 352, 21));
        frmMain->setMenuBar(menuBar);
        mainToolBar = new QToolBar(frmMain);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        frmMain->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(frmMain);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        frmMain->setStatusBar(statusBar);

        retranslateUi(frmMain);

        QMetaObject::connectSlotsByName(frmMain);
    } // setupUi

    void retranslateUi(QMainWindow *frmMain)
    {
        frmMain->setWindowTitle(QApplication::translate("frmMain", "\320\257 \320\277\320\276\320\273\320\272\320\276\320\262\320\275\320\270\320\272, \320\270 \321\202\321\213 \320\275\320\260 \320\261\320\265\320\273\320\276\320\274 \320\272\320\276\320\275\320\265!", 0));
        cmdEdit->setText(QApplication::translate("frmMain", "\320\241\320\276\321\205\321\200\320\260\320\275\320\270\321\202\321\214", 0));
        lblFIO->setText(QApplication::translate("frmMain", "\320\244\320\230\320\236:", 0));
        lblRANK->setText(QApplication::translate("frmMain", "\320\227\320\262\320\260\320\275\320\270\320\265:", 0));
        lblREASON->setText(QApplication::translate("frmMain", "\320\237\321\200\320\270\321\207\320\270\320\275\320\260 \321\203\320\261\321\213\321\202\320\270\321\217:", 0));
        cboRANK->clear();
        cboRANK->insertItems(0, QStringList()
         << QApplication::translate("frmMain", "\320\240\321\217\320\264\320\276\320\262\320\276\320\271", 0)
         << QApplication::translate("frmMain", "\320\225\321\204\321\200\320\265\320\271\321\202\320\276\321\200", 0)
         << QApplication::translate("frmMain", "\320\241\320\265\321\200\320\266\320\260\320\275\321\202", 0)
         << QApplication::translate("frmMain", "\320\241\321\202\320\260\321\200\321\210\320\270\320\271 \321\201\320\265\321\200\320\266\320\260\320\275\321\202", 0)
         << QApplication::translate("frmMain", "\320\237\321\200\320\260\320\277\320\276\321\200\321\211\320\270\320\272", 0)
         << QApplication::translate("frmMain", "\320\241\321\202\320\260\321\200\321\210\320\270\320\271 \320\277\321\200\320\260\320\277\320\276\321\200\321\211\320\270\320\272", 0)
         << QApplication::translate("frmMain", "\320\233\320\265\320\271\321\202\320\265\320\275\320\260\320\275\321\202", 0)
         << QApplication::translate("frmMain", "\320\241\321\202\320\260\321\200\321\210\320\270\320\271 \320\273\320\265\320\271\321\202\320\265\320\275\320\260\320\275\321\202", 0)
         << QApplication::translate("frmMain", "\320\232\320\260\320\277\320\270\321\202\320\260\320\275", 0)
         << QApplication::translate("frmMain", "\320\234\320\260\320\271\320\276\321\200", 0)
         << QApplication::translate("frmMain", "\320\237\320\276\320\264\320\277\320\276\320\273\320\272\320\276\320\262\320\275\320\270\320\272", 0)
         << QApplication::translate("frmMain", "\320\237\320\276\320\273\320\272\320\276\320\262\320\275\320\270\320\272", 0)
         << QApplication::translate("frmMain", "\320\223\320\265\320\275\320\265\321\200\320\260\320\273-\320\274\320\260\320\271\320\276\321\200", 0)
         << QApplication::translate("frmMain", "\320\223\320\265\320\275\320\265\321\200\320\260\320\273-\320\273\320\265\320\271\321\202\320\265\320\275\320\260\320\275\321\202", 0)
         << QApplication::translate("frmMain", "\320\223\320\265\320\275\320\265\321\200\320\260\320\273-\320\277\320\276\320\273\320\272\320\276\320\262\320\275\320\270\320\272", 0)
         << QApplication::translate("frmMain", "\320\223\320\265\320\275\320\265\321\200\320\260\320\273 \320\260\321\200\320\274\320\270\320\270", 0)
         << QApplication::translate("frmMain", "\320\234\320\260\321\200\321\210\320\260\320\273 \320\240\320\244", 0)
        );
        cboREASON->clear();
        cboREASON->insertItems(0, QStringList()
         << QApplication::translate("frmMain", "\320\236\321\202\320\277\321\203\321\201\320\272 \320\276\321\201\320\275\320\276\320\262\320\275\320\276\320\271", 0)
         << QApplication::translate("frmMain", "\320\236\321\202\320\277\321\203\321\201\320\272 \320\277\320\276 \320\273\320\270\321\207\320\275\321\213\320\274 \320\276\320\261\321\201\321\202\320\276\321\217\321\202\320\265\320\273\321\214\321\201\321\202\320\262\320\260\320\274", 0)
         << QApplication::translate("frmMain", "\320\236\321\202\320\277\321\203\321\201\320\272 \320\272\320\260\320\275\320\270\320\272\321\203\320\273\321\217\321\200\320\275\321\213\320\271", 0)
         << QApplication::translate("frmMain", "\320\223\320\276\321\201\320\277\320\270\321\202\320\260\320\273\321\214", 0)
         << QApplication::translate("frmMain", "\320\232\320\276\320\274\320\260\320\275\320\264\320\270\321\200\320\276\320\262\320\272\320\260", 0)
         << QApplication::translate("frmMain", "\320\236\321\201\320\262\320\276\320\261\320\276\320\266\320\264\320\265\320\275\320\276 \320\276\321\202 \320\267\320\260\320\275\321\217\321\202\320\270\320\271 \320\277\320\276 \320\261\320\276\320\273\320\265\320\267\320\275\320\270", 0)
         << QApplication::translate("frmMain", "\320\233\320\260\320\267\320\260\321\200\320\265\321\202", 0)
        );
    } // retranslateUi

};

namespace Ui {
    class frmMain: public Ui_frmMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FRMMAIN_H
